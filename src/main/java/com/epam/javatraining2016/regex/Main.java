package com.epam.javatraining2016.regex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
  static String inFilename = "D:\\tmp\\phones.in.txt";
  static String outFilename = "D:\\tmp\\phones.out.txt";

  public static void main(String[] args) throws FileNotFoundException, IOException {
    PhoneFilter phoneFilter = new PhoneFilter();

    try (BufferedReader reader = new BufferedReader(new FileReader(inFilename));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outFilename))) {

      Set<String> phones = reader.lines().map(phoneFilter)
          .flatMap((List<String> phonesInLine) -> phonesInLine.stream())
          .collect(Collectors.toSet());
      for (String phone : phones) {
        writer.write(phone);
        writer.newLine();
      }
    }
  }
}
