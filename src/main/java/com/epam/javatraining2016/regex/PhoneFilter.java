package com.epam.javatraining2016.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneFilter implements Function<String, List<String>> {
  static Pattern pattern = Pattern
      .compile("(?:^|\\s)\\+(\\d)\\((\\d{3})\\)\\s+(\\d{3})\\s+(\\d{2})\\s+(\\d{2})(?=$|\\s)");


  @Override
  public List<String> apply(String t) {
    ArrayList<String> retval = new ArrayList<String>();
    Matcher matcher = pattern.matcher(t);
    while (matcher.find()) {
      retval.add(String.format("%s%s%s%s%s", matcher.group(1), matcher.group(2), matcher.group(3),
          matcher.group(4), matcher.group(5)));
    }
    return retval;
  }
}
