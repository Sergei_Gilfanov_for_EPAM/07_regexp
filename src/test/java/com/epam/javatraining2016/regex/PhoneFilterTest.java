package com.epam.javatraining2016.regex;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PhoneFilterTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  public void shouldFindNumber() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("a +1(234) 567 89 01 b");
    assertEquals("12345678901", retval.get(0));;
  }

  @Test
  public void shouldFindExact() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("+1(234) 567 89 01");
    assertEquals("12345678901", retval.get(0));;
  }

  @Test
  public void shouldFindAtEndOfLine() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("a +1(234) 567 89 01");
    assertEquals("12345678901", retval.get(0));;
  }

  @Test
  public void shouldFindAtStartOfLine() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("+1(234) 567 89 01 asdf");
    assertEquals("12345678901", retval.get(0));;
  }

  @Test
  public void shouldRequireSpaceBefore() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("a+1(234) 567 89 01");
    assertEquals(0, retval.size());;
  }

  @Test
  public void shouldRequireSpaceAfter() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("+1(234) 567 89 01b");
    assertEquals(0, retval.size());;
  }

  @Test
  public void shouldFindMultiple() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("+1(234) 567 89 01 +2(345) 678 90 12");
    assertEquals("12345678901", retval.get(0));
    assertEquals("23456789012", retval.get(1));
  }

  @Test
  public void shouldNotFindTooLong() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("a +1(234) 567 89 011 b");
    assertEquals(0, retval.size());;
  }

  @Test
  public void shouldSeekSpaceBetween() {
    PhoneFilter filter = new PhoneFilter();
    List<String> retval = filter.apply("a +1(234) 567 89 01+2(345) 678 90 12");
    assertEquals(0, retval.size());;
  }
}
